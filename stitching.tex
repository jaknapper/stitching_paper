\documentclass{optica-article}

\journal{opticajournal} % for journals or Optica Open

\articletype{Research Article}

\usepackage{subcaption}
\usepackage{todonotes}
\usepackage{multirow}
\usepackage{booktabs}

\begin{document}

\title{Automated sample scanning for intelligent microscopy: An open-source toolkit to improve the efficiency of scanning and stitching}

\author{Joe Knapper\authormark{1*}, Freya Whiteford\authormark{1}, Daniel Rosen\authormark{2}, William Wadsworth\authormark{3} and Richard Bowman\authormark{1}}

\address{\authormark{1}School of Physics and Astronomy, University of Glasgow, UK\\
\authormark{2}Baylor College of Medicine, Texas, USA\\
\authormark{3}University of Bath, Bath, UK
}

\email{\authormark{*}joe.knapper@glasgow.ac.uk} %% email address is required; see note below about the corresponding author designation

% use {asbstract*} to suppress the copyright line. Copyright information will be added in production

\begin{abstract*} 
Automated microscopy has become an essential part of healthcare and biology labs, producing huge datasets over many fields of view. We present a pipeline to efficiently collect and process this data, automatically returning a large-area reconstruction of samples. Image collection is optimised by characterising the motion of the stage and identifying areas of interest, intelligently updating the scan path based on the LUV* fingerprint of each field of view. Images are then efficiently positioned and stitched based on their pairwise correlations, automatically producing a composite pyramidal TIFF with minimal user input. Correlation caching greatly reduces the time and memory requirements, allowing real-time previews of large scans with memory requirements under 1 GB, and adjustments to the input images or settings to be processed in seconds. Program outputs can feedback on the characteristics of the hardware, improving the performance of future scans.

\end{abstract*}

\listoftodos

%%%%%%%%%%%%%%%%%%%%%%%%%%  body  %%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Automated microscopes and translation stages have greatly increased the throughput of microscopy research, allowing large-area, high resolution datasets to be produced with minimal user intervention \cite{Collins, FIJI, Guo2020}. Users often require these multi-Gigabyte datasets to be assembled into a single composite image. This is achieved through tiling (positioning the images) and stitching (combining the images) into a large-area recreation of the sample. While microscopes with automated translation stages generally store the position of each image in the metadata, this positioning is rarely accurate enough to directly locate images.

We present a set of Python programs to fully automate the scanning, tiling and stitching of large-area 2D scans. Users need only to position the sample of interest within the starting field of view. Based on this, empty areas are identified, updating the future scan path to avoid unwanted regions. Non-rectangular, irregularly-spaced and incomplete scans are all supported by the stitching, and scans containing z-stacks have the most focused image extracted for use. Initial image positions are automatically predicted from image metadata, or estimated from user input. 

These estimates are used to predict overlapping image pairs, greatly increasing the efficiency of the program. The correlation is used to align pairs of images with sub-pixel precision. Alignment quality is assessed from the shape of the correlation map, correlation value, and agreement with stage estimates. Thresholding is performed automatically, removing the need for subjective user input. Once a dataset is processed, the slow and computationally-intensive steps are cached, allowing adjustments or additional images to be processed in seconds.

OpenFlexure Stitching was inspired by use in low-cost systems with limited resources, and is capable of tiling multi-Gigabyte scans with sub-pixel accuracy on a 2GB Raspberry Pi 4. The speed is comparable to other microscopy tiling programs, while caching allows near real-time previews of live scanning. Images were collected autonomously on the OpenFlexure Microscope, an automated, open-source, 3D printed microscope costing 250 USD in parts \cite{Collins}. Datasets tiled include a range of diagnostic pathology and cytology samples, with sufficient resolution and contrast for diagnosis.

As an open-source Python program, users can freely adapt or contribute to the tiling and stitching methods. The work on image offset identification also forms the basis of Simultaneous Localisation And Mapping (SLAM), building the framework for intelligent, closed-loop scanning. Figure \ref{fig:examples} shows examples of biological samples automatically scanned and tiled using our pipeline.

\begin{figure}
	\centering
 \begin{subfigure}[t]{0.3\textwidth}
			  \centering
	 \includegraphics[angle=90,origin=c, width=\textwidth,height=\textheight,keepaspectratio=true]{figures/pine_leaf_centres.jpg}
	 \caption{}
	 \label{fig:pine}
 \end{subfigure}\hspace{1em}%
 \begin{subfigure}[t]{0.3\textwidth}
	 \centering
	 \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/ricestem_centres_resized.jpeg}
	 \caption{}
	 \label{fig:rice}
 \end{subfigure}	
 \begin{subfigure}[t]{0.3\textwidth}
	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/prostate.jpg}
	\caption{}
	\label{fig:prostate}
\end{subfigure}	
 \begin{subfigure}[b]{0.49\textwidth}
			  \centering
	 \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/stitched_figure.jpg}
	 \caption{}
	 \label{fig:pap}
 \end{subfigure}
 \begin{subfigure}[b]{0.49\textwidth}
	 \centering
	 \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/stitched.jpg}
	 \caption{}
	 \label{fig:earthworm}
 \end{subfigure}	
 \caption{Example datasets scanned and tiled automatically through our pipeline on an OpenFlexure Microscope using a 0.65 NA objective. The scan path and stitching thresholds were determined automatically by our software, reducing the need for user intervention. a) Pine leaf b) Rice stem c) Prostate core d) Pap smear e) Earthworm cross section}
 \label{fig:examples}
\end{figure}

\section{Image Collection}

\subsection{Sample identification and path optimisation}

For exploration of biological samples, a regular rectangular grid is rarely the most optimal. Capturing images of empty regions wastes time and storage space, and autofocusing on these regions will reposition the optics at an arbitrary position above the sample. Following consecutive failed autofocus procedures, the system may be sufficiently misaligned to fail to refocus when the sample of interest re-enters the field of view.

We optimise the automated scanning of samples by detecting the nature of the current field of view during a scan, and adjusting future movements according to the label. Emulating the workflow of manual microscopy, we use the colour of the region of interest to distinguish sample from background. Prior to the scan, a background image is collected, and its characteristic distribution in the LUV* colourspace mapped. The LUV* colourspace was chosen as it attempts to group colours based on human perception of colour similarity, rather than the biology-based grouping of RGB \cite{Cheung2012}. Subsequent fields of view also have their LUV* distribution extracted, and the degree of overlap within a set number of standard deviations is found.

If there is significant overlap, the region is labelled as background. Otherwise, the FOV contains something assumed to be of interest, and so is refocused on, captured, and the scan path adjusted accordingly. If scan site $S_{x,y}$ is labelled as containing sample, then sites $S_{x-1,y}$, $S_{x+1,y}$, $S_{x,y-1}$ and $S_{x,y+1}$ are all appended to the path, and the order of future movements adjusted accordingly. Two example images of features from automated scans are shown in figures \ref{fig:sample} and \ref{fig:background}. The LUV distribution of these images are shown in figures \ref{fig:plotsample} and \ref{fig:plotbackground}, and compared to the distribution of calibration background images.

\begin{figure}
   	\centering
	\begin{subfigure}[t]{0.35\textwidth}
         		\centering
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/sample.jpeg}
		\caption{}
		\label{fig:sample}
	\end{subfigure}\hspace{1em}%
	\begin{subfigure}[t]{0.35\textwidth}
		\centering
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/sample_and_background.jpeg}
		\caption{}
		\label{fig:background}
	\end{subfigure}	
	\begin{subfigure}[b]{0.49\textwidth}
         		\centering
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/plot_sample.pdf}
		\caption{}
		\label{fig:plotsample}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/plot_background.pdf}
		\caption{}
		\label{fig:plotbackground}
	\end{subfigure}	
	\caption[Examples of images of sample and sample with background, with their respective LUV pixel distribution.]{Examples of images of sample and sample with background captured on the OpenFlexure Microscope, with their respective LUV pixel distribution. (a) An image of a pine leaf section, used to build up the composite image in figure \ref{fig:pine}. (b) An image of a rice stem section, used to build up the composite image in figure \ref{fig:rice}. (c) The LUV pixel distribution of a background image (not shown) and the sample image shown in (a). There is little overlap between the distributions, especially in L and U channels. (d) The LUV pixel distribution of a background image (not shown) and the sample image shown in (b). Note the presence of background next to and within the cells causes significant overlap in the distributions, requiring different thresholding to distinguish from pure background.}
	\label{fig:backgrounddetect}
\end{figure}

The resulting scans are often faster, more reliable and contain fewer images than a scan over a user-chosen rectangular area. However, the scans are often non-rectangular, and may contain empty areas within the scanned region. Examples of these unfilled scans are shown in figures \ref{fig:rice} and \ref{fig:earthworm}. These paths can be more challenging to tile. While this can be overcome by including virtual background images when tiling \cite{Lu2018}, this delays the production of the composite image, is less reliable, and may mislead the user into considering an area as empty rather than unscanned.

We instead present a Python program to automatically tile overlapping microscopy images, without reliance on regular scan patterns.

\section{Image tiling and stitching}

\subsection{Image and parameters input}

Our program allows user control over many parameters when beginning tiling. Options to downsample the input and resulting composite image are separate, allowing each to be selected individually. Downsampling input images increases the speed of the image alignment, while downsampling the resulting image reduces the stitching time. As the results of tiling are cached permanently by the program, this allows rapid previewing of the output image or ongoing scan, while the larger, slower, high-resolution image needs only to be produced once.

Where an initial estimation of image coordinates is provided, users can also set a threshold for the estimated overlap area before a pair of images are treated as overlapping. This gives the user the option to exclude diagonally-overlapping images which may only have a few pixels in common, or include them as an additional source of alignment.

\subsection{Initial alignment}

Many automated microscopes store the stage coordinates in the metadata of each image. Where a stage has been calibrated for its link between stage and pixel coordinates, this directly provides an estimation of the image positions and overlaps. The procedure in OpenFlexure software which calibrates between motor steps, physical stage position and image pixels can be applied to other translation stages, and details are given in supplementary information \ref{appendix:CSM}. After running, the stitching program also returns an improved estimate of this Camera-Stage Mapping (CSM) as a 2$\times$2 matrix. This enables an estimation of image locations on future scans from the same hardware, even if the hardware itself only stores stage positions.

If image capture locations are unknown, the user can also define that sequential images overlap, and so each image will only be assumed to overlap with the images captured immediately before and after it. In the case that the scan path is fully unknown, all images are initially assumed to overlap. Testing all images for overlapping regions is slow, and so is recommended to be used on a subset of the scan, and the results used to estimate positions of all images in the scan.

\subsection{Offset estimation}
\label{subsection:OffsetEst}

For each pair of images estimated to overlap, $f$ and $g$, the cross-correlation is calculated according to
\begin{equation}
	f \star g = IFT(\mathcal{F}(\bar{f})\mathcal{G}(g)),
\end{equation}
where IFT is the inverse Fourier transform, $\star$ is the cross-correlation, $\mathcal{F}$ and $\mathcal{G}$ are the Fourier transforms of their respective functions, and $\bar{f}$ is the complex conjugate of $f$ \cite{Kapinchev2015}. This cross-correlation produces a 2D map of correlation scores, each corresponding to the level of alignment if that point was the image offset. The sub-pixel location of the peak is extracted as the centre of mass of the peak. Due to the periodic boundary conditions of a 2D cross correlation, for an unpadded image, this peak corresponds to one of four potential offsets. This symmetry is broken by extracting the proposed region of overlap from each image according to the four candidate offsets. The highest correlation between the two regions of overlap is selected as the proposed offset.

In the ideal case, the resulting 2D correlation map would be a single delta function at the point of optimal overlap. Multiple regions of similarity and underlying structure may add additional false peaks, or smear the peak into a wider, less-defined ridge. The proposed offset is stored, along with parameters to quantify the confidence in the offset. For the stitched images included in this work, the extracted metrics were the discrepancy between the offsets estimated by the stage and by the correlations (lower suggests better agreement), and the area of the correlation map with a correlation value above the top 10\% of the value range. A smaller area suggests a narrower, single peak, implying greater confidence in the correlation map. Figure \ref{fig:correlations} shows example correlations for a successful and failed pair tile. 

\begin{figure}
   	\centering
	\begin{subfigure}[b]{0.49\textwidth}
         		\centering
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/corr_success.png}
		\caption{}
		\label{fig:corrsuccess}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/corr_failed.png}
		\caption{}
		\label{fig:corrfailure}
	\end{subfigure}	
	\begin{subfigure}[b]{0.49\textwidth}
         		\centering
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/3d_success.png}
		\caption{}
		\label{fig:3dsuccess}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{figures/3d_failed.png}
		\caption{}
		\label{fig:3dfailed}
	\end{subfigure}	
	\caption[2D and 3D maps of the correlation score for every possible offset between two images]{2D and 3D maps of the correlation score for every possible offset between two images, for both a successful (a, c) and failed (b, d) image pair. (a) A 2D colourmap of the correlation between a successfully aligned pair of images. The peak is visible at (640, 90). (b) A 2D colourmap of the correlation between an unsuccessfully aligned pair of images. (c) A 3D plot of the correlation between a successfully aligned pair of images. The red plane at $z = 0.5$ is the threshold which filters for narrow, unique peaks. Here, $>$99.9\% of possible overlaps fall below this cutoff. (d) A 3D plot of the correlation between an unsuccessfully aligned pair of images. The red plane at $z = 0.5$ is the threshold which filters for narrow, unique peaks. Here, 58\% of possible overlaps fall below this cutoff, suggesting a poor fit.}
	\label{fig:correlations}
\end{figure}

As open-source software, users have the option to adapt this code for other confidence metrics. While only 2D thresholding is included in this work, the addition of further thresholds would allow the user greater control over the resulting image.

The pairs of overlapping images, their estimated offset, and the offset correlation metrics are cached at this point. They are stored as a JSON file in the scan folder, which is automatically checked should the program be run again. If the input parameters are unchanged, or if the cached data is still useful (for example, if a smaller scan was cached, and now forms part of a larger scan to be tiled), the cache is loaded, removing the need for the computationally-expensive correlations to be recalculated.

Users have the option to prioritise memory or speed when running this program. When prioritising memory, the only images held in memory are the two being compared at that point. This keeps the memory required far below the 2GB available on the lowest RAM Raspberry Pi 4s. When prioritising speed, relevant images and their Fourier transforms are held in memory, speeding up the tiling but requiring additional memory.

\subsection{Offset filtering}

The offsets for each pair of images proposed by the correlations now require filtering to remove erroneous correlations. In existing solutions, this is achieved using user-defined thresholds set at the start of the program \cite{FIJI}. However, suitable thresholds vary depending on the appearance of the sample, and so are hard to estimate in advance. By determining the thresholds after calculating the correlations, suitable thresholds and their impact can be better estimated.

As the program attempts to position images according to every accepted offset, the cutoff threshold should be chosen carefully. Accepting incorrect offsets not only causes those images to be misplaced, but also affects the overall stage to image positioning matrix. This requires a threshold sufficiently high to eliminate all unsuccessful pairs, even at the expense of other correct overlaps.

The consequence of raising the threshold too far is that some images --- or clusters of images --- will become cut off from the rest of the scan. In this case, disconnected images will be placed according to the overall fit from stage coordinates to image location. Due to uncertainty in stage position and local variance in camera-stage mapping, this will be less accurate than placing according to a correlation. Therefore, a suitable threshold is one between the lowest incorrect offset, and the highest cutoff which keeps a conjoined path.

The thresholds to use can be set manually or automatically. In the manual case, the confidence metrics are plotted for the user, allowing them to visualise the placement of their threshold instead of arbitrarily selecting a cutoff. All pairs above this threshold are accepted as valid offsets. A least squares fit then positions all images according to their accepted offsets. The quality of the overall fit is measured according to the root mean squared error between the correlations and the fitted positions.

To automatically set the threshold, this RMS error is calculated for the fit according to every possible combination of thresholds in $x$ and $y$. The thresholds with the lowest RMS error while still connecting every image are taken as the optimal cutoffs, and the tiled image assembled from these.

Additional offset filtering can be performed at this point, in order to identify any failed offsets which have passed through the previous thresholding. The program includes the option to apply the RANSAC (RANdom SAmple Consensus) algorithm to the pair offsets, filtering out any with significant deviation from the overall stage-to-image fit \cite{scikit}. Other filters can be implemented into the program, taking advantage of the significant number of data processing algorithms freely available in Python from libraries such as scipy and scikit \cite{scipy, scikit}.

\subsection{Image stitching}

Once images have been optimally positioned according to the accepted offsets, several output options are available. Images can be placed into the composite image directly, in order of image capture. This produces a mosaic-like output, with sharp boundaries between images. This can be advantageous for identifying which image contributed a certain feature, while making artefacts such as vignetting clear.

While many smoothing methods between images are available, and improve the apparent appearance of the composite image, many of them attempt to average between pixel intensities or otherwise create data. For our applications, only pixel values that have been directly captured are used. To account for vignetting and lower resolution towards the edge of each field of view, each pixel in the final image can be assigned based on its proximity to the centre of contributing images. In regions of overlapping images, the composite image is assigned the pixel value from the pixel closest to the centre of its individual tile. This method uses the high-quality image centres to produce the composite image, while the edges of the field of view are only used for image alignment.

Finally, the optimised image positions can be exported as a config.txt file, formatted such that it can be loaded into Fiji Stitching for composite image production if preferred \cite{FIJI}. The optimal CSM matrix is also returned, allowing improved movement calibration and position estimation in future scans. The scan input settings are cached, allowing the program to quickly determine whether the results of any steps can be reloaded from a previous tiling attempt.

Images can be exported as either a JPEG or pyramidal OME TIFF. JPEGs are one of the most common image formats, and so a very versatile file for smaller scans. For larger scans, loading giga-pixel images as JPEGs is computationally expensive. Pyramidal TIFFs split the composite image into multiple scales and resolutions, allowing them to be saved and opened with minimal processing power.

\section{Results}

All image tiling and stitching was performed on the same Lenovo Thinkpad Yoga with 16 GB installed RAM with an Intel(R) Core(TM) i5-8265U CPU @ 1.60GHz, running Windows 10 64 bit.

Figure \ref{fig:examples} shows scans completed automatically through this pipeline. Users were required to position the sample and run the relevant scripts, but otherwise no manual input was required. The improved efficiency in scan path is shown by the regions of black within the scan --- with a user-defined rectangle to scan, these areas would have been imaged, increasing the time and memory requirements.

\begin{table}[t]
	\resizebox{\textwidth}{!}{%
	\centering
	\begin{tabular}{ |c|c|c|c|c|c| } 
	\hline
	 Sample & Pine (\ref{fig:pine}) & Rice (\ref{fig:rice}) & Prostate (\ref{fig:prostate}) & Pap smear (\ref{fig:pap}) & Earthworm (\ref{fig:earthworm}) \\ \hline
	 Image count (rectangular) & 42 & 162 & 266 & 660 & 63 \\ 
	 Image count (sample-only) & 35 & 85 & 40 & 525 & 50 \\ 
	 Time and memory reduction & 17\% & 48\% & 85\% & 20\% & 21\% \\ 
	 \hline
	\end{tabular}}
	\caption{The images captured in each scan shown in figure \ref{fig:examples}, compared to the number of images that would have been required if a full rectangular scan path was used.}
	\label{table:savings}
	\end{table}

The increase in scan efficiency depends on the shape of the sample; near-rectangular objects such as figure \ref{fig:pine} will have limited improvement, while irregular shapes such as figures \ref{fig:rice} and \ref{fig:pap} will have much greater improvement. Table \ref{table:savings} shows these increases, saving up to 85\% of the time and memory required. While this improvement would be lessened by taking care to align the sample edges to the scan axes, this further increases the effort required by the user prior to running a scan.

To benchmark performance, OpenFlexure Stitching was run in both `Memory' and `Speed' modes. The `Memory' setting limited the available RAM to 700 MB, considered a reasonable allocation on low-resource devices such as the Raspberry Pi. The time and memory requirements to tile various scans into Pyramidal TIFFs are shown in table \ref{table:speed}.

\begin{table}[t]
\resizebox{\textwidth}{!}{%
\centering
\begin{tabular}{|c|c|c|cc|cc|}
\hline
\multirow{2}{*}{\begin{tabular}[c]{@{}c@{}}Sample\\ Description\end{tabular}} & \multirow{2}{*}{\begin{tabular}[c]{@{}c@{}}Image\\ Count\end{tabular}} & \multirow{2}{*}{\begin{tabular}[c]{@{}c@{}}Input Folder\\ Size (MB)\end{tabular}} & \multicolumn{2}{c|}{Speed} & \multicolumn{2}{c|}{Memory} \\ \cline{4-7} 
 &  &  & \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}Peak memory\\ use (MB)\end{tabular}} & \begin{tabular}[c]{@{}c@{}}Time\\ (mm:ss)\end{tabular} & \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}Peak memory\\ use (MB)\end{tabular}} & \begin{tabular}[c]{@{}c@{}}Time\\ (mm:ss)\end{tabular} \\ \hline
Rice stem & 96 & 394 & \multicolumn{1}{c|}{1867} & 6:42 & \multicolumn{1}{c|}{630} & 10:37 \\ \hline
Breast tissue & 82 & 339 & \multicolumn{1}{c|}{1864} & 4:48 & \multicolumn{1}{c|}{628} & 6:58 \\ \hline
Prostate core & 40 & 142 & \multicolumn{1}{c|}{1862} & 3:17 & \multicolumn{1}{c|}{626} & 4:23 \\ \hline
Pap smear & 525 & 1810 & \multicolumn{1}{c|}{4350} & 51:25 & \multicolumn{1}{c|}{651} & 61:13 \\ \hline
\end{tabular}}
\caption{The time and memory requirements to tile and stitch various scans in OpenFlexure Stitching, in both the Speed and Memory settings.}
\label{table:speed}
\end{table}

For comparison with table \ref{table:speed}, the 96-image rice stem scan was stitched into a JPEG in Fiji Stitching. As all images were held in memory at once during the tiling, Fiji Stitching required a peak 9.2 GB of RAM, taking 9 minutes 15 seconds. This is slower than OpenFlexure Stitching running in `Speed' mode, while also requiring around 5$\times$ the RAM. For larger scans, such as the pap smear, the large amount of input data would make some existing stitching methods such as Fiji Stitching unsuitable.

Once a scan has been tiled, the parameters from the correlations are cached, allowing for thresholds to be adjusted and additional images to be appended in seconds, rather than requiring the program to be fully rerun for each change. This allows for a realtime stitched preview of an ongoing scan, as shown in a video in supplementary material.

The accuracy in tiling and stitching was assessed by scanning the same earthworm cross-section sample with 0.25 NA, 0.40 NA, 0.65 NA and 0.85 NA RMS objectives. For each change to the hardware, calibration between the stage, image and physical space was rerun (supplementary materials \ref{appendix:CSM}), then used to extract the height of the same sample based on the stitched images. These dimensions are shown in table \ref{table:change_NA}. The automated scanning was repeated using the 0.65 NA objective for a total of 5 scans using the same hardware. Each scan began at a different position, and scanned the sample in a unique, automated path. The dimensions of the sample in the resulting composite image are also included in \ref{table:change_NA}. The range of apparent sample sizes varies by around 3\% of the standard deviation of measurements. The high level of agreement in sample size (at both varying and consistent magnification) indicates consistent success at both automatically scanning and tiling the sample.

Multiple samples, at various magnifications, scales and capture quality, have been scanned and stitched from this pipeline. Datasets are included in supplementary information, and include red blood cell smears, pap smears, breast tissue biopsy, prostate biopsy, rice stem, stomach biopsy, pine leaf and oesophagus biopsy.

For samples with a clear contrast between sample and background (such as H\&E stained biopsies) and sufficient features to allow tiling, we expect this pipeline ---- or individual components of it --- to be applicable to improving the throughput and automation of microscopy scanning.

\begin{table}[t]
\begin{center}
\begin{tabular}{ |c|c|c|c| } 
 \hline
 Magnification & NA & Image Count & Height (\textmu m) \\ \hline
 10$\times$ & 0.25 & 5 & 1657 \\ 
 20$\times$ & 0.40 & 16 & 1668 \\ 
 40$\times$ & 0.65 & 51 & 1659 \\ 
 40$\times$ & 0.65 & 51 & 1660 \\ 
 40$\times$ & 0.65 & 51 & 1659 \\ 
 40$\times$ & 0.65 & 51 & 1655 \\ 
 40$\times$ & 0.65 & 50 & 1654 \\ 
 40$\times$ & 0.65 & 51 & 1657* \\ 
 60$\times$ & 0.85 & 130 & 1630 \\ 
 \hline
\end{tabular}
\caption{The height of the same earthworm cross-section, imaged at various magnifications with automated path planning, and stitched in our program. Physical pixel size calibrated according to MICAT \cite{Micat}, and distance measured in QuPath \cite{Bankhead2017}. * marks a mean of 5 scans using the same objective.}
\label{table:change_NA}
\end{center}
\end{table}

\section{Conclusion}

This pipeline, written in Python, allows ``glass to image" processing of a range of samples with minimal user intervention. Calibration between stage, pixel and physical dimensions is performed on an accessible, low-cost USAF 1951 resolution target. Images are collected in an automated, intuitive way, efficiently skipping regions without data for an up to 85\% decrease in time and memory requirements.

Multiple tools have been published for the tiling and stitching of microscopy datasets, including open source solutions, and proprietary slide scanners with integrated stitching. These solutions are generally intended for a completed large area scan, designed to be run once. Appending new images, or adjusting input settings, requires the full program to be rerun from scratch. Our approach of caching the intensive stages of tiling into a JSON file allows previous results to be combined with new images and settings, rather than being overwritten. This makes realtime previews of scans much more efficient to produce, essentially as byproducts of the large area stitched image.

Existing solutions often require file format conversion, specific file naming conventions, or supplementary data files \cite{ASHLAR,FIJI}. For large datasets, this refactoring can take considerable time or expertise. Time or memory demands can also scale poorly with dataset size, making these solutions unsuitable for Raspberry Pi-based pipelines.

OpenFlexure Stitching allows intuitive, streamlined image stitching from large area scans. User inputs allow metadata to be seamlessly imported, while parameters such as downsampling allow control based on the application. Thresholds can be set automatically, or else set by the user based on the data, rather than estimated before the tiling is performed. Caching allows changes to the thresholds, without requiring the full program be rerun. Images can be in any format supported by the OpenCV library \cite{opencv}, making this broadly applicable without requiring conversion or pre-processing.

The identification, filtering and use of overlapping regions between multiple images has applications beyond image tiling. Detecting the overlaps between images is also fundamental to the problem of Simultaneous Localisation and Mapping (SLAM), a common problem in robotics with sensors \cite{SLAM}. Closed-loop movements, live sample tracking and the localisation within an unknown environment are all desirable in automated microscopes, and are all enabled from the framework presented here.

Future work will involve combining these scripts into a singular program, suitable for untrained users to scan and tile samples on an automated microscope with minimal training. The self-calibration routines will be used as part of OpenFlexure's distributed manufacturing, ensuring quality control of remotely-assembled devices in clinical settings. Individual components of this pipeline are also released as open-source software, allowing use in other automation and microscopy projects.

\section*{Data acquisition statement}

Imaging of human tissue samples was performed at the Michael E. DeBakey VA Medical Center, Houston, in April 2023. Ethical approval was granted under IRB Protocol Number H-24175.

\section*{Acknowledgements}

Travel to support the data acquisition was funded by Baylor College of Medicine Internal Funding Only and the National Institutes of Health [R01 CA181275].

%%%%%%%%%% If using BibTeX:
\bibliography{sample}

\pagebreak
\begin{center}
\textbf{\large Supplementary Materials}
\end{center}
%%%%%%%%%% Merge with supplemental materials %%%%%%%%%%
%%%%%%%%%% Prefix a "S" to all equations, figures, tables and reset the counter %%%%%%%%%%
\setcounter{equation}{0}
\setcounter{figure}{0}
\setcounter{table}{0}
\setcounter{page}{1}
\setcounter{section}{0}
\makeatletter
\renewcommand{\theequation}{S\arabic{equation}}
\renewcommand{\thefigure}{S\arabic{figure}}
%%%%%%%%%% Prefix a "S" to all equations, figures, tables and reset the counter %%%%%%%%%%


\section{Stage characterisation}
\label{appendix:CSM}

The connections between pixel size, stage step size and physical distance are all required for full characterisation of a translation stage. These vary between devices, and should be characterised on each new instrument. The conversion between stage steps and pixels can be characterised by a series of movements in both $x$ and $y$, using the correlation between images to find the pixel shift. This motion will be affected by backlash and non-orthogonality between image and stage axes, and so is characterised according to 

\begin{equation}
\begin{bmatrix}
x\textsubscript{image}\\
y\textsubscript{image}
\end{bmatrix}
=
\begin{bmatrix}
a_x & b_x\\
b_y & a_y
\end{bmatrix}
\begin{bmatrix}
x\textsubscript{motor}\\
y\textsubscript{motor}
\end{bmatrix}
-
\begin{bmatrix}
c_x\\
c_y
\end{bmatrix},
\end{equation}
where $a$ is the conversion between intended motion, $b$ is the parasitic motion in the other axis, and $c$ is the backlash in the system. This calibration can be performed on any device with endpoints for moving and capturing images, and is released independently as the Camera-Stage-Mapping procedure \cite{csm}.

Calibration of the physical size of each pixel is performed on an image of a USAF 1951 resolution target, extracting the pixel sizes of known-width periodic features. MICAT uses the known periodicity of USAF resolution target features to identify decreasingly spaced groups of features, converting between their measured pixel width and published physical size \cite{Micat}. USAF 1951 resolution targets are low-cost and accessible, available from a range of suppliers for around 200 GBP. The characterisation of the stage is automated and open, allowing users to adapt the procedure and extract the data for other hardware.

The OpenFlexure Microscope includes multiple autofocus routines, developed for a range of applications including high power scanning, fine adjustment, focus on a hard edge, and closed loop corrections \cite{Knapper2022}. Automated translation stages often attempt to minimise time spent refocusing by collecting a ``focus map" from the current sample \cite{Guo2020}. Each scan can begin by refocusing at multiple $xy$ sites in the scan region. Assuming the sample is flat and the translation stage is linear, the focused position of all scan sites can then be interpolated, saving time over refocusing at each site.

The assumption that the parasitic creep in $z$ during an $xy$ move is linear is not always valid. Due to the parallelogram-based stage design, samples on the OpenFlexure Microscope are translated over a sphere cap \cite{Collins}. While this disallows simple interpolation, this is also predictable behaviour which can be modelled. The re-centring extension, also in Python, locates the centre of the stage's range of motion, while also characterising the dependence of $z$ position from the $xy$ distance from this centre \cite{height_map}.

As shown in figure \ref{fig:heightmap}, this dependence over a large range of motion is reliable. Once measured for a stage, future scans at low NA (and so higher depth of field) can skip the need to refocus at every scan site. Scans at higher NA can reduce the range required to scan for the focused position, improving throughput. This stage mechanics calibration can also indicate if nominally similar hardware performs similarly, or be used to identify damage to a stage.

\begin{figure}
   	\centering
	\includegraphics[width=1\textwidth]{figures/heightmap_full.png}
	\caption{Map of recorded focused height against $xy$ position for an OpenFlexure Microscope scan  of a flat sample. Sample is a standard microscope slide with whiteboard marker ink to provide a thin sample to focus on. In the 3D plot, white dots mark the recorded position, while the surface and joining lines connect the nearest points for clarity. In the subplots showing the 2D projections, only the recorded positions are shown. All positions estimated from the motor position and conversion to physical distance according to OpenFlexure calibration routines.}
	\label{fig:heightmap}
\end{figure}


\end{document}